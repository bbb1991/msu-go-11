package main

import (
	"sort"
	"strconv"
)

func ReturnInt() int8 {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [...]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(arr []int) string {

	var result = ""

	for _, val := range arr {
		result += strconv.Itoa(val)
	}

	return result
}

func MergeSlices(a1 []float32, a2 []int32) []int {

	var result = make([]int, 0, len(a1)+len(a2))

	for _, val := range a1 {
		result = append(result, int(val))
	}

	for _, val := range a2 {
		result = append(result, int(val))
	}

	return result
}

func GetMapValuesSortedByKey(input map[int]string) []string {

	var keys = make([]int, 0, len(input))

	for key := range input {
		keys = append(keys, key)
	}

	var result = make([]string, 0, len(input))

	sort.Ints(keys)

	for _, key := range keys { // Very strange behaviour
		result = append(result, input[key])
	}

	return result
}
